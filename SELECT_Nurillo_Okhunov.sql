--THERE ARE SOLUTIONS BELOW. PLEASE, AFTER UNCOMMENTING THE CODE, CLICK THE CURSOR OF THE MOUSE SOMEWHERE.
--BECAUSE "pgAdmin4" MIGHT NOT COMPILE THE CODE IN THE FIRST TRY. WELL, I USE pgAdmin4 Tools>Query INSTEAD OF DBEAVER FOR SQL CODING.

-- 1) Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?
-- SELECT s.store_id, CONCAT(s.first_name, ' ', s.last_name) AS staff_name, SUM(p.amount) AS revenue
-- FROM payment p
-- JOIN staff s ON p.staff_id = s.staff_id
-- WHERE p.payment_date >= '2017-01-01' AND p.payment_date < '2018-01-01'
-- GROUP BY s.store_id, s.staff_id
-- HAVING SUM(p.amount) = (
--     SELECT MAX(total_revenue)
--     FROM (
--         SELECT s.store_id, s.staff_id, SUM(p.amount) AS total_revenue
--         FROM payment p
--         JOIN staff s ON p.staff_id = s.staff_id
--         WHERE p.payment_date >= '2017-01-01' AND p.payment_date < '2018-01-01'
--         GROUP BY s.store_id, s.staff_id
--     ) AS revenue_by_store
--     WHERE revenue_by_store.store_id = s.store_id
-- )

--2) Which five movies were rented more than the others, and what is the expected age of the audience for these movies?
-- SELECT f.title, COUNT(*) AS rental_count, AVG(f.rental_duration) AS expected_age
-- FROM film f
-- JOIN inventory i ON f.film_id = i.film_id
-- JOIN rental r ON i.inventory_id = r.inventory_id
-- GROUP BY f.film_id
-- ORDER BY rental_count DESC
-- LIMIT 5

--3) Which actors/actresses didn't act for a longer period of time than the others?
SELECT CONCAT(a.first_name, ' ', a.last_name) AS actor_name, MAX(f.release_year) - MIN(f.release_year) AS years_since_last_movie
FROM actor a
JOIN film_actor fa ON a.actor_id = fa.actor_id
JOIN film f ON fa.film_id = f.film_id
GROUP BY a.actor_id
ORDER BY years_since_last_movie DESC